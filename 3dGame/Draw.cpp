#include "Point.h"
#include "Global.h"
#include <iostream>
#include <Gl/glut.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include "Engine.h"

using namespace std;




void drawProjectile(){
	glColor3f(1.0, 0.0, 0.0);
	glPushMatrix();
	glTranslatef(projectile.x,projectile.y,projectile.z);
	glutSolidCube(0.5);
	glPopMatrix();


	
}

void drawFire() {
	if(projectile.init){
	glColor3f(1.0,0.0,0.0);
	glPushMatrix();
	glTranslatef(spaceshipPosition.x,spaceshipPosition.y,spaceshipPosition.z);
	glutWireCube(cubeRadius);
	glPopMatrix();
	}
}

void drawGround() {

	glColor3f(0.9f, 0.9f, 0.9f);


	glBegin(GL_QUADS);
		glVertex3f(-100.0f, 0.0f, -20.0f);
		glVertex3f(-100.0f, 0.0f,  20.0f);
		glVertex3f( 200.0f, 0.0f,  20.0f);
		glVertex3f( 200.0f, 0.0f, -20.0f);
	glEnd();


	glColor3f(0,0,0);

	for(int i=-100;i<100;i++){
		i+=9;
	glBegin(GL_LINES);
	glVertex3f(i,0,-20);
	glVertex3f(i,0,20);
	glEnd();
	}
	
}