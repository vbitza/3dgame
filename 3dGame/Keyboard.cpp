#include <Gl/glut.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <iostream>
#include "Point.h"
#include "Keyboard.h"
#include "Engine.h"
#include "Global.h"

using namespace std;

void keyOperations (void) {  

	if(keyStates['a']==true)
	{
		isMovingRight = false;
		isMovingLeft = !isMovingLeft;
	}
	if(keyStates['a']==false)
		isMovingLeft=false;
	if(keyStates['d']==true)
	{
		isMovingLeft = false;
		isMovingRight = !isMovingRight;
	}
	if(keyStates['d']==false)
		isMovingRight=false;
	if(keyStates['w']==true)
		isMovingUp = !isMovingUp;
	if(keyStates['w']==false)
		isMovingUp=false;
	if(keyStates['s']==true)
		isMovingDown = !isMovingDown;
	if(keyStates['s']==false)
		isMovingDown=false;
	if(keyStates['1']==true)
			shoot();


}  


void keyPress(unsigned char key, int x, int y) 
{
	keyStates[key] = true;  
}

void keyUp (unsigned char key, int x, int y) 
{  
	keyStates[key] = false;   
} 