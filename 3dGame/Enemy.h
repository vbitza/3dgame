#ifndef ENEMY_H
#define ENEMY_H


struct Enemy {
	float x;
	float y;
	float z;
	bool state;
	int hp;
	bool hit;
};

#endif//ENEMY_H