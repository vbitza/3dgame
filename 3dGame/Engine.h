#ifndef ENGINE_H
#define ENGINE_H

void spawnEnemy();
void checkCollision();
void update();
void glTimer();
void shoot();
void updateObjects();


#endif ENGINE_H 