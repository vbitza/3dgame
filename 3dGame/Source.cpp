//SURSA:  lighthouse3D:  http://www.lighthouse3d.com/tutorials/glut-tutorial/keyboard-example-moving-around-the-world/ 

#include <Gl/glut.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <iostream>
#include<math.h>
#include "Keyboard.h"
#include "Engine.h"
#include "Global.h"
#include "Draw.h"
using namespace std;


void changeSize(int w, int h)
{

// Prevent a divide by zero, when window is too short
// (you cant make a window of zero width).
if (h == 0)
h = 1;
float ratio = w * 1.0 / h;

// Use the Projection Matrix
glMatrixMode(GL_PROJECTION);

// Reset Matrix
glLoadIdentity();

// Set the viewport to be the entire window
glViewport(0, 0, w, h);

// Set the correct perspective.
gluPerspective(45.0f, ratio, 0.1f, 100.0f);

// Get Back to the Modelview
glMatrixMode(GL_MODELVIEW);
}




void renderScene(void) {

	glEnable(GL_BLEND);
	keyOperations();
	updateObjects();
	// Clear Color and Depth Buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Reset transformations
	glLoadIdentity();
	// Set the camera
	gluLookAt(	spaceshipPosition.x-5, spaceshipPosition.y+3 , spaceshipPosition.z,
			spaceshipPosition.x+5, spaceshipPosition.y,  spaceshipPosition.z,
			0.0f, 1.0f,  0.0f);

        // Draw ground

	glPushMatrix();
		glTranslatef(-moveF,0,0);
		drawGround();
		glPopMatrix();
	//glPopMatrix();

	//glPopMatrix();
		

	glPushMatrix();
	glColor3f(0,0,0);
	glTranslatef(spaceshipPosition.x,spaceshipPosition.y,spaceshipPosition.z);
	glutWireCube(1);
	glPopMatrix();
	spawnEnemy();

	if(projectile.state==true)
		{
			drawProjectile();
			
	
	}
	if(projectile.init==true)
		drawFire();
	
	checkCollision();

	glutSwapBuffers();
	glTimer();
}



int main(int argc, char **argv) {

	// init GLUT and create window

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(100,100);
	glutInitWindowSize(320,320);
	glutCreateWindow("Lighthouse3D - GLUT Tutorial");

	// register callbacks
	glutDisplayFunc(renderScene);
	glutReshapeFunc(changeSize);
	glutIdleFunc(glTimer);
	glutKeyboardFunc(keyPress);
	glutKeyboardUpFunc(keyUp);
	
	// OpenGL init
	glEnable(GL_DEPTH_TEST);

	// enter GLUT event processing cycle
	glutMainLoop();

	return 1;
}

