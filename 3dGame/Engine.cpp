#include <Gl/glut.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <iostream>
#include "Global.h"
#include "Engine.h"
#include "Draw.h"

using namespace std;


void checkCollision()
{
	if(projectile.state == true){
		if(projectile.x >= enemy.x-2 && projectile.x <= enemy.x+2) {
			if(projectile.z >= enemy.z-1 && projectile.z <= enemy.z +1)
			{


				projectile.state = false;
				projectile.x=0;
				projectile.y=0;
				projectile.z=0;
				enemy.hit=true;
				enemy.hp--;
				if(enemy.hp==0)
					enemy.state = false;
			}
		}
	}
}

void spawnEnemy(){
	if(!enemy.state){
		while(true){
			bool collides = false;
			Point tempEnemyPosition = { rand()%30+50 , spaceshipPosition.y , rand()%20};
			if(collides == false){
				enemy.x = tempEnemyPosition.x;
				enemy.y = tempEnemyPosition.y;
				enemy.z = tempEnemyPosition.z;
				enemy.state = true;
				enemy.hp = 2;
				break;
			}
		}
	}
	glPushMatrix();
	glTranslatef(enemy.x,enemy.y,enemy.z);
	glColor3f(0,0,1);
	if(enemy.hp>1)
	glutSolidCube(2);
	else if(enemy.hp==1)
		glutWireCube(2);
	glPopMatrix();
}

void update()
{
	double var=0.1;
	if(moveF>=100)
		moveF = -50;
	moveF+=var;

	if(isMovingLeft) spaceshipPosition.z -= 0.1 ;

	if(isMovingRight) spaceshipPosition.z += 0.1;

	if(isMovingDown) spaceshipPosition.x -= 0.1;

	if(isMovingUp) spaceshipPosition.x += 0.1;

}

void glTimer() {
	// Update display
	//glutPostRedisplay();
	// Reset timer
	double timeSinceStart = glutGet(GLUT_ELAPSED_TIME);
	double deltaTime = timeSinceStart - oldTimeSinceStart;


	update();
	glutPostRedisplay();
}

void shoot(void)
{

	if(projectile.state == false)
	{
		projectile.x=spaceshipPosition.x;
		projectile.y=spaceshipPosition.y;
		projectile.z=spaceshipPosition.z;
		projectile.state = true;
		projectile.init = true;
		cubeRadius=0.03;
	

	} 
}


void updateObjects(void)
{
	if(enemy.x<spaceshipPosition.x)
		enemy.state=false;
	if(projectile.x>enemy.x)
	{
	
		projectile.state=false;
	}
	if (projectile.state == true)
	{
		projectile.x += 0.5;
		if (projectile.x > 100 ) 
		{
			projectile.state = false;
		}

	}

	if(enemy.hit==true)
	{

		cubeRadius2+=0.1;
		glPushMatrix();
		glTranslatef(enemy.x,enemy.y,enemy.z);
		glutSolidCube(cubeRadius);
		glPopMatrix();
		if(cubeRadius>10)
			enemy.hit=false;
	}
	if(projectile.init == true)
	{
		if(cubeRadius==0.03)
			sizevar = 0.005;
		if(cubeRadius>=0.5)
			sizevar = -0.005;
		cubeRadius +=sizevar;
		if(cubeRadius<=0){
			projectile.init=false;
			
		}
	}

	

	if (enemy.state == true)
	{
		enemy.x -= 0.05;
		if(enemy.x < -100)
		{
			enemy.state = false;
		}
	}

}

