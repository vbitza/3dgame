#ifndef GLOBAL_H
#define GLOBAL_H
#include "Point.h"
#include "Projectile.h"
#include "Enemy.h"

extern Point spaceshipPosition;
extern Projectile projectile;
extern Enemy enemy;

extern double cubeRadius;
extern double cubeRadius2;
extern double sizevar;

extern double moveF;

extern bool isMovingLeft;
extern bool isMovingRight;
extern bool isMovingUp;
extern bool isMovingDown;
extern bool* keyStates;

extern double speed;
extern double oldTimeSinceStart; 



#endif//GLOBAL_H